const tinhChuVi = function () {
  const chieuDai = document.getElementById(`chieudai`).value;
  const chieuRong = document.getElementById(`chieurong`).value;
  const chuVi = (Number(chieuDai) + Number(chieuRong)) * 2;

  document.getElementById(
    `result-ex4`
  ).innerHTML = `Chu vi hình chữ nhật: ${chuVi}`;
};

const tinhDienTich = function () {
  const chieuDai = document.getElementById(`chieudai`).value * 1;
  const chieuRong = document.getElementById(`chieurong`).value * 1;
  const dienTich = chieuDai * chieuRong;

  document.getElementById(
    `result-ex4`
  ).innerHTML = `Diện tích hình chữ nhật: ${dienTich}`;
};
